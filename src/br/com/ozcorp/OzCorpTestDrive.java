package br.com.ozcorp;
/**
 * Impress�o dos M�todos do projeto da empresa OzCorp
 * 
 * @author Nycolas de L. Vieira
 * 
 */

public class OzCorpTestDrive {
	public static void main(String[] args) {
		
		// Cria��o do funcion�rio analista
		Analista analista = new Analista("J�o", "18.975.249-6", "654.325.315-12", "6786521", "jaopaizao@corp.com", 
				"1221", TipoSanguineo.AB_NEGATIVO, Sexo.MASCULINO, 4,
				new Departamento("Financeiro", " (FI)", new Cargo("Analista", 7_500)));
		
		// Impress�o das informa��es
		System.out.println("Dados do Funcion�rio: ");
		System.out.println("Nome:            " + analista.getNome());
		System.out.println("RG:              " + analista.getRg());
		System.out.println("CPF:             " + analista.getCpf());
		System.out.println("Matr�cula:       " + analista.getMatricula());
		System.out.println("Email:           " + analista.getEmail());
		System.out.println("Senha:           " + analista.getSenha());
		System.out.println("Departamento:    " + analista.getDepartamento().getNome() + analista.getDepartamento().getSigla());
		System.out.println("Cargo:           " + analista.getDepartamento().getCargo().getTitulo());
		System.out.println("S�lario Base:    " + analista.getDepartamento().getCargo().getSalarioBase());
		System.out.println("N�vel de Acesso: " + analista.getNivelAcess());
		System.out.println("Tipo Sangu�neo:  " + analista.getTipoSanguineo().nome);
		System.out.println("Sexo:            " + analista.getSexo().nome);
		
		System.out.println(); // PULA LINHA
		
		// Cria��o do funcion�rio engenheiro
		Engenheiro engenheiro = new Engenheiro("Suellen", "67.185.168-8", "654.321.854-46", "8446841", "sullinda@corp.com",
				"321!", TipoSanguineo.A_POSITIVO, Sexo.FEMININO, 3, 
				new Departamento("Fabrica��o", " (FB)", new Cargo("Engenheira", 8_000)));
		
		// Impress�o das informa��es
		System.out.println("Dados do Funcion�rio: ");
		System.out.println("Nome:            " + engenheiro.getNome());
		System.out.println("RG:              " + engenheiro.getRg());
		System.out.println("CPF:             " + engenheiro.getCpf());
		System.out.println("Matr�cula:       " + engenheiro.getMatricula());
		System.out.println("Email:           " + engenheiro.getEmail());
		System.out.println("Senha:           " + engenheiro.getSenha());
		System.out.println("Departamento:    " + engenheiro.getDepartamento().getNome() + engenheiro.getDepartamento().getSigla());
		System.out.println("Cargo:           " + engenheiro.getDepartamento().getCargo().getTitulo());
		System.out.println("S�lario Base:    " + engenheiro.getDepartamento().getCargo().getSalarioBase());
		System.out.println("N�vel de Acesso: " + engenheiro.getNivelAcess());
		System.out.println("Tipo Sangu�neo:  " + engenheiro.getTipoSanguineo().nome);
		System.out.println("Sexo:            " + engenheiro.getSexo().nome);
		
		System.out.println(); // PULA LINHA
		
		// Cria��o do funcion�rio gerente
		Gerente gerente = new Gerente("Ronaldo", "13.245.543-7", "354.343.867-12", "8543584", "ronaldofenomeno@corp.com", 
				"FNM", TipoSanguineo.O_POSITIVO, Sexo.MASCULINO, 2, 
				new Departamento("Financeiro", " (FI)", new Cargo("Gerente", 9_500)));
		
		// Impress�o das informa��es
		System.out.println("Dados do Funcion�rio: ");
		System.out.println("Nome:            " + gerente.getNome());
		System.out.println("RG:              " + gerente.getRg());
		System.out.println("CPF:             " + gerente.getCpf());
		System.out.println("Matr�cula:       " + gerente.getMatricula());
		System.out.println("Email:           " + gerente.getEmail());
		System.out.println("Senha:           " + gerente.getSenha());
		System.out.println("Departamento:    " + gerente.getDepartamento().getNome() + gerente.getDepartamento().getSigla());
		System.out.println("Cargo:           " + gerente.getDepartamento().getCargo().getTitulo());
		System.out.println("S�lario Base:    " + gerente.getDepartamento().getCargo().getSalarioBase());
		System.out.println("N�vel de Acesso: " + gerente.getNivelAcess());
		System.out.println("Tipo Sangu�neo:  " + gerente.getTipoSanguineo().nome);
		System.out.println("Sexo:            " + gerente.getSexo().nome);
		
		System.out.println(); // PULA LINHA
		
		// Cria��o do funcion�rio secret�rio
		Secretario secretario = new Secretario("Fernand�o", "76.564.245-4", "754.687.254-21", "4851219", "fernandaosonome@corp.com",
				"YGA", TipoSanguineo.B_NEGATIVO, Sexo.OUTRO, 1,
				new Departamento("Vendas", " (VD)", new Cargo("Secret�rio", 2_500)));
		
		// Impress�o das informa��es
		System.out.println("Dados do Funcion�rio: ");
		System.out.println("Nome:            " + secretario.getNome());
		System.out.println("RG:              " + secretario.getRg());
		System.out.println("CPF:             " + secretario.getCpf());
		System.out.println("Matr�cula:       " + secretario.getMatricula());
		System.out.println("Email:           " + secretario.getEmail());
		System.out.println("Senha:           " + secretario.getSenha());
		System.out.println("Departamento:    " + secretario.getDepartamento().getNome() + secretario.getDepartamento().getSigla());
		System.out.println("Cargo:           " + secretario.getDepartamento().getCargo().getTitulo());
		System.out.println("S�lario Base:    " + secretario.getDepartamento().getCargo().getSalarioBase());
		System.out.println("N�vel de Acesso: " + secretario.getNivelAcess());
		System.out.println("Tipo Sangu�neo:  " + secretario.getTipoSanguineo().nome);
		System.out.println("Sexo:            " + secretario.getSexo().nome);
		
		System.out.println(); // PULA LINHA
		
		// Cria��o do funcion�rio diretor
		Diretor diretor = new Diretor("Karen", "45.321.841-5", "453.231.354-54", "4791469", "kahlinda@corp.com", 
				"@#$", TipoSanguineo.O_NEGATIVO, Sexo.FEMININO, 0, 
				new Departamento("Presidencia", " (PR)", new Cargo("Diretora", 50_000)));
		
		// Impress�o das informa��es
		System.out.println("Dados do Funcion�rio: ");
		System.out.println("Nome:            " + diretor.getNome());
		System.out.println("RG:              " + diretor.getRg());
		System.out.println("CPF:             " + diretor.getCpf());
		System.out.println("Matr�cula:       " + diretor.getMatricula());
		System.out.println("Email:           " + diretor.getEmail());
		System.out.println("Senha:           " + diretor.getSenha());
		System.out.println("Departamento:    " + diretor.getDepartamento().getNome() + diretor.getDepartamento().getSigla());
		System.out.println("Cargo:           " + diretor.getDepartamento().getCargo().getTitulo());
		System.out.println("S�lario Base:    " + diretor.getDepartamento().getCargo().getSalarioBase());
		System.out.println("N�vel de Acesso: " + diretor.getNivelAcess());
		System.out.println("Tipo Sangu�neo:  " + diretor.getTipoSanguineo().nome);
		System.out.println("Sexo:            " + diretor.getSexo().nome);
	}	
}
