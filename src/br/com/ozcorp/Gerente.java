package br.com.ozcorp;

/**
 * Classe Gerente extendida a classe Funcionario
 * 
 * @author Nycolas de L. Vieira
 * 
 */

public class Gerente extends Funcionario{

	// Construdor extendido com a classe Funcionario
	public Gerente(String nome, String rg, String cpf, String matricula, String email, String senha,
			TipoSanguineo tipoSanguineo, Sexo sexo, int nivelAcess, Departamento departamento) {
		
		super(nome, rg, cpf, matricula, email, senha, tipoSanguineo, sexo, nivelAcess, departamento);
	}

}
