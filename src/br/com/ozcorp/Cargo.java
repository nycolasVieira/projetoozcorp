package br.com.ozcorp;

/**
 * Classe Cargo do projeto da empresa OzCorp
 * 
 * @author Nycolas de L. Vieira
 * 
 */

public class Cargo {
	
	// ATRIBUTOS
	private String titulo;
	private int salarioBase;
		
	// CONSTRUTOR
	public Cargo(String titulo, int salarioBase) {
		this.titulo = titulo;
		this.salarioBase = salarioBase;
	}
	
	// GETTERS && SETTERS

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getSalarioBase() {
		return salarioBase;
	}

	public void setSalarioBase(int salarioBase) {
		this.salarioBase = salarioBase;
	}
	
}
