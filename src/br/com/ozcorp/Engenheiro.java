package br.com.ozcorp;

/**
 * Classe Engenheiro extendida a classe Funcionario
 * 
 * @author Nycolas de L. Vieira
 * 
 */

public class Engenheiro extends Funcionario{

	// Construdor extendido com a classe Funcionario
	public Engenheiro(String nome, String rg, String cpf, String matricula, String email, String senha,
			TipoSanguineo tipoSanguineo, Sexo sexo, int nivelAcess, Departamento departamento) {
		
		super(nome, rg, cpf, matricula, email, senha, tipoSanguineo, sexo, nivelAcess, departamento);
	}

}
