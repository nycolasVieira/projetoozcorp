package br.com.ozcorp;

/**
 * Enum Sexo do projeto da empresa OzCorp
 * 
 * @author Nycolas de L. Vieira
 * 
 */

public enum Sexo {
	
	// Op��es para informar no funcion�rio
	MASCULINO("Masculino"), FEMININO("Feminino"), OUTRO("Outro");
	
	public String nome;
	
	Sexo(String nome) {
		this.nome = nome;
	}
}

