package br.com.ozcorp;

/**
 * Enum TipoSanguineo do projeto da empresa OzCorp
 * 
 * @author Nycolas de L. Vieira
 * 
 */

public enum TipoSanguineo {

	// Op��es para informar no funcion�rio
	A_POSITIVO ("A positivo (A+)"),   A_NEGATIVO  ("A negativo (A-)"), 
	B_POSITIVO ("B positivo (B+)"),   B_NEGATIVO  ("B negativo (B-)"),
	O_POSITIVO ("O positivo (O+)"),   O_NEGATIVO  ("O negativo (O-)"),
	AB_POSITIVO("AB positivo (AB+)"), AB_NEGATIVO ("AB negativo (AB-)");
	
	public String nome;
	
	TipoSanguineo(String nome) {
		this.nome = nome;
	}
	
}
