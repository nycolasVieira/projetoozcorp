package br.com.ozcorp;

/**
 * Classe Secretário extendida a classe Funcionario
 * 
 * @author Nycolas de L. Vieira
 * 
 */

public class Secretario extends Funcionario{

	// Construdor extendido com a classe Funcionario
	public Secretario(String nome, String rg, String cpf, String matricula, String email, String senha,
			TipoSanguineo tipoSanguineo, Sexo sexo, int nivelAcess, Departamento departamento) {
		
		super(nome, rg, cpf, matricula, email, senha, tipoSanguineo, sexo, nivelAcess, departamento);
	}
	
}
